Website
=======
https://developer.nvidia.com/cg-toolkit

License
=======
proprietary license (see the file LICENSE-nvidia-cg-toolkit.txt either in i386-linux-gnu/lib or in x86_64-linux-gnu/lib)

Version
=======
3.1_April2012

Source
======
Cg-3.1_April2012_x86_64.tgz (sha256: e8ff01e6cc38d1b3fd56a083f5860737dbd2f319a39037528fb1a74a89ae9878)

Cg-3.1_April2012_x86.tgz    (sha256: cef3591e436f528852db0e8c145d3842f920e0c89bcfb219c466797cb7b18879)
